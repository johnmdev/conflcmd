#! /usr/bin/env python2
#
# conflcmd/connection.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import base64
import json
import requests
import traceback

class Connection:
    """Connection to the Confluence site with support for Basic
    authorization and GET and POST actions.
    """

    def __init__(self, baseurl, username=None, password=None):
        self.baseurl = baseurl
        self.username = username
        if username and password:
            self.userpassb64 = base64.b64encode("%s:%s" % (username, password))
        else:
            self.userpassb64 = None

    def _get_headers(self):
        headers = {
            "X-Atlassian-Token": "no-check",
        }
        if self.userpassb64:
            headers["Authorization"] = "Basic %s" % self.userpassb64
        return headers

    def delete(self, pathqs, headers=None):
        headers = headers and headers.copy() or {}
        headers.update(self._get_headers())
        url = "%s%s" % (self.baseurl, pathqs)
        return requests.delete(url, headers=headers)

    def get(self, pathqs, headers=None):
        headers = headers and headers.copy() or {}
        headers.update(self._get_headers())
        url = "%s%s" % (self.baseurl, pathqs)
        return requests.get(url, headers=headers)

    def post(self, path, headers=None, files=None, data=None):
        headers = headers and headers.copy() or {}
        headers.update(self._get_headers())
        url = "%s%s" % (self.baseurl, path)
        return requests.post(url, headers=headers, files=files, data=data)

    def put(self, path, headers=None, files=None, data=None):
        headers = headers and headers.copy() or {}
        headers.update(self._get_headers())
        url = "%s%s" % (self.baseurl, path)
        return requests.put(url, headers=headers, files=files, data=data)
