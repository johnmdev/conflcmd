#! /usr/bin/env python2
#
# conflcmd/misc.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import os.path
import subprocess

def getenpass(serverurl, username, passph):
    """Get password from enpass file.
    """
    ENPASSFILE = os.path.expanduser("~/.conflcmd/enpass")
    pargs = ["enpass", "get", "-f". "-", ENPASSFILE, serverurl, username]
    p = subprocess.Popen(pargs,
        stdin=subprocess.PIPE,
        stdout=subprocess.STDOUT, stderr=subprocess.STDERR)
    pout, perr = p.communicate(passph)
    if p.return_code == 0:
        return pout.strip()
