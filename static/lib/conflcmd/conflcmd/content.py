#! /usr/bin/env python2
#
# conflcmd/content.py

# Copyright (c) 2019 John Marshall. All right reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

"""Collection of classes supporting Confluence objects:
Space
+ Page
  +- Attachment
  +- Comment

Content is the base class providing low-level access to the
Confluence objects.
"""

import json
import traceback

from .connection import Connection

from errors import Error

class Site:
    """Base object for working with a Confluence site.
    """

    def __init__(self, baseurl, username=None, password=None):
        self.conn = Connection(baseurl, username, password)

    def get_content(self, contentid):
        return Content(self, contentid)

    def get_conn(self):
        return self.conn

    def get_space(self, spacekey):
        try:
            space = Space(self, spacekey)
            if space.get_id() == None:
                return Error("space (%s) does not exist" % str(spacekey))
            return space
        except:
            #traceback.print_exc()
            return Error("Site.get_space: unexpected error")

    def get_space_keys(self):
        try:
            resp = self.get_conn().get("/rest/api/space?limit=10000")
            if resp.status_code == 200:
                j = resp.json()
                return [jj["key"] for jj in j["results"]]
        except:
            #traceback.print_exc()
            return None

    def get_space_names(self):
        try:
            resp = self.get_conn().get("/rest/api/space?limit=10000")
            j = resp.json()
            return [jj["name"] for jj in j["results"]]
        except:
            #traceback.print_exc()
            return None

class Content:
    """Generic content object.

    id (contentid) is retrieved/set lazily. It must also be obtained
    using the helper Content.get_id().
    """

    def __init__(self, site, contentid=None):
        self.site = site
        self.__id = contentid
        self.name = None
        self.type = None

    def _get_meta(self):
        """Load meta data information: id, title, type.
        """
        pass

    def _get_id(self):
        """Get content id from server.
        """
        return None

    def get(self, expand=None):
        """Return json information for object identified by content id.
        """
        pathqs = "/rest/api/content/%s" % self.get_id()
        if expand:
            pathqs += "?expand=%s" % ",".join(expand)
        return self.get_conn().get(pathqs)

    def get_content(self, expand=None):
        """Return whatever best corresponds to "content" (e.g., page
        body, attachment file content). This is not metadata.
        """
        pass

    def get_conn(self):
        """Return connection object.
        """
        return self.site.get_conn()

    def get_id(self):
        """Return content id.
        """
        if self.__id == None:
            self.__id = self._get_id()
        return self.__id

    def get_json(self, expand=None):
        try:
            resp = self.get(expand)
            return resp.json()
        except:
            #traceback.print_exc()
            return None

    def get_site(self):
        return self.site

class Space(Content):
    """Space object.
    """

    def __init__(self, site, spacekey):
        Content.__init__(self, site)
        self.spacekey = spacekey

    def _get_id(self):
        """Get contentid from server.
        """
        try:
            resp = self.get_conn().get("/rest/api/space/%s" % self.spacekey)
            j = resp.json()
            return j["id"]
        except:
            #traceback.print_exc()
            return None

    def create_page(self, parentpagename, pagename, content, data=None):
        """Create page. Set parentpagename to "" for top-level page.
        """
        try:
            resp = self.get()
            headers = {
                "Content-Type": "application/json",
            }
            data = {
                "type": "page",
                "title": pagename,
                "space": {
                    "key": self.spacekey,
                },
                "body": {
                    "storage": {
                        "value": content,
                        "representation": "storage",
                    },
                },
            }

            if parentpagename != "":
                parentpage = self.get_page(parentpagename)
                data["ancestors"] = [{"id": parentpage.get_id()}]

            resp = self.get_conn().post("/rest/api/content",
                headers=headers, data=json.dumps(data))
            if resp.status_code != 200:
                return Error("%s: %s" % (resp.status_code, resp.message))
        except:
            #traceback.print_exc()
            return Error("Space.create_page: unexpected error")

    def delete_page(self, pagename):
        """Delete page.
        """
        try:
            page = self.get_page(pagename)
            url = "/rest/api/content/%s" % (page.get_id())
            resp = self.get_conn().delete(url)
            if resp.status_code not in [200, 204]:
                return Error("%s: %s" % (resp.status_code, resp.message))
        except:
            #traceback.print_exc()
            return Error("Space.delete_page: unexpected error")

    def get(self):
        """Does this work for Space?
        """
        return None

    def get_page(self, pagename, trashed=False):
        """Return Page object.
        """
        try:
            page = Page(self, pagename, trashed)
            if page.get_id() == None:
                return Error("page (%s) does not exist" % str(pagename))
            return page
        except:
            #traceback.print_exc()
            return Error("Space.get_page: unexpected error")

    def get_page_names(self, limit=10000, trashed=False):
        """Return list of pages by name.
        """
        try:
            url = "/rest/api/content?spaceKey=%s&limit=%s" % (self.spacekey, limit)
            if trashed:
                url = "%s&status=trashed" % url
            resp = self.get_conn().get(url)
            j = resp.json()
            return [jj["title"] for jj in j["results"]]
        except:
            #traceback.print_exc()
            return None

    def purge_page(self, pagename):
        """Purge page.
        """
        try:
            page = self.get_page(pagename, trashed=True)
            url = "/rest/api/content/%s?status=trashed" % page.get_id()
            resp = self.get_conn().delete(url)
            if resp.status_code not in [200, 204]:
                return Error("%s: %s" % (resp.status_code, resp.message))
        except:
            traceback.print_exc()
            return Error("Space.purge_page: unexpected error")

class Page(Content):
    """Page object (with Space parent).
    """

    def __init__(self, space, pagename, trashed=False):
        Content.__init__(self, space.get_site())
        self.space = space
        self.pagename = pagename
        self.trashed = trashed

    def __get_current_version(self):
            resp = self.get()
            return resp.json()["version"]["number"]

    def _get_id(self):
        """Get contentid from server.
        """
        try:
            url = "/rest/api/content?spaceKey=%s&title=%s" % (self.space.spacekey, self.pagename)
            if self.trashed:
                url = "%s&status=trashed" % url
            resp = self.get_conn().get(url)
            j = resp.json()
            return j["results"][0]["id"]
        except:
            #traceback.print_exc()
            return None

    def add_attachment(self, attname, content, comment, data=None):
        try:
            data = data and data.copy() or {}
            data["comment"] = comment
            url = "/rest/api/content/%s/child/attachment" % self.get_id()
            resp = self.get_conn().post(url, files={"file": (attname, content)}, data=data)
            if resp.status_code not in [200, 204]:
                j = resp.json()
                return Error("%s: %s" % (resp.status_code, j["message"]))
        except:
            #traceback.print_exc()
            return Error("Page.add_attachment: unexpected error")

    def add_comment(self, content, data=None):
        erro = Error("not implemented")
        return erro

    def delete_attachment(self, attname):
        """Delete attachment.
        """
        try:
            att = self.get_attachment(attname)
            url = "/rest/api/content/%s" % att.get_id()
            resp = self.get_conn().delete(url)
            if resp.status_code != 200:
                j = resp.json()
                return Error("%s: %s" % (resp.status_code, j["message"]))
        except:
            #traceback.print_exc()
            return Error("Page.delete_attachment: unexpected error")

    def delete_version(self, version):
        """Delete attachment.
        """
        try:
            url = "/rest/api/content/%s/version/%s" % (self.get_id(), version)
            url = "/rest/experimental/content/%s/version/%s" % (self.get_id(), version)
            resp = self.get_conn().delete(url)
            if resp.status_code not in [200, 204]:
                j = resp.json()
                return Error("%s: %s" % (resp.status_code, j["message"]))
        except:
            #traceback.print_exc()
            return Error("Page.delete_version: unexpected error")

    def get_attachment(self, attname):
        """Return Attachment object.
        """
        try:
            att = Attachment(self, attname)
            if att.get_id() == None:
                return Error("attachment (%s) does not exist" % str(attname))
            return att
        except:
            #traceback.print_exc()
            return Error("Page.get_attachment: unexpected error")

    def get_attachments(self):
        """Return list of Attachment objects.
        """
        try:
            resp = self.get_conn().get("/rest/api/content/%s/child/attachment" % self.get_id())
            j = resp.json()
            return [Attachment(self, jj["title"]) for jj in j["results"]]
        except:
            #traceback.print_exc()
            return None

    def get_content(self):
        """Get content/body.
        """
        try:
            pathqs = "/rest/api/content/%s?expand=body.storage" % self.get_id()
            resp = self.get_conn().get(pathqs)
            return resp.json()["body"]["storage"]["value"]
        except:
            #traceback.print_exc()
            return None

    def get_version_count(self):
        """Get page version count.
        """
        try:
            pathqs = "/rest/api/content/%s/history" % self.get_id()
            resp = self.get_conn().get(pathqs)
            return int(resp.json()["lastUpdated"]["number"])
        except:
            #traceback.print_exc()
            return None

    def get_view(self):
        """Get content/body.
        """
        try:
            pathqs = "/rest/api/content/%s?expand=body.view" % self.get_id()
            resp = self.get_conn().get(pathqs)
            resp.encoding = "utf-8"
            return resp.json()["body"]["view"]["value"]
        except:
            #traceback.print_exc()
            return None

    def get_attachment_names(self):
        """Return list of attachments by name.
        """
        try:
            resp = self.get_conn().get("/rest/api/content/%s/child/attachment" % self.get_id())
            j = resp.json()
            return [jj["title"] for jj in j["results"]]
        except:
            #traceback.print_exc()
            return None

    def update(self, content, comment, data=None):
        """Update page body.
        """
        try:
            version = self.__get_current_version()
            headers = {
                "Content-Type": "application/json",
            }
            data = data and data.copy() or {}
            data.update({
                "id": self.get_id(),
                "type": "page",
                "title": self.pagename,
                "space": {
                    "key": self.space.spacekey,
                },
                "body": {
                    "storage": {
                        "value": content,
                        "representation": "storage",
                    },
                    "comment": comment,
                },
                "version": {
                    "number": version+1,
                },
            })
            if comment:
                data["version"]["message"] = comment
            resp = self.get_conn().put("/rest/api/content/%s" % self.get_id(),
                headers=headers, data=json.dumps(data))
            if resp.status_code != 200:
                j = resp.json()
                return Error("%s: %s" % (resp.status_code, j["message"]))
        except:
            #traceback.print_exc()
            return Error("Page.update: unexpected error")

class Attachment(Content):
    """Attachment object (with Page parent).
    """

    def __init__(self, page, attname):
        Content.__init__(self, page.get_site())
        self.page = page
        self.attname = attname

    def _get_id(self):
        """Get contentid from server.
        """
        try:
            resp = self.get_conn().get("/rest/api/content/%s/child/attachment" % self.page.get_id())
            j = resp.json()
            for jj in j["results"]:
                if jj["title"] == self.attname:
                    return jj["id"]
        except:
            #traceback.print_exc()
            return None

    def get_content(self):
        """Get content/file.
        """
        try:
            pathqs = "/rest/api/content/%s" % self.get_id()
            resp = self.get_conn().get(pathqs)
            dllink = resp.json()["_links"]["download"]
            return site.get_conn().get(dllink).text
        except:
            #traceback.print_exc()
            return None

    def update(self, content, comment, data=None):
        """Update content/file.
        """
        try:
            data = data and data.copy() or {}
            if comment:
                data["comment"] = comment
            url = "/rest/api/content/%s/child/attachment/%s/data" % (self.page.get_id(), self.get_id())
            resp = self.get_conn().post(url, files={"file": (self.attname, content)}, data=data)
            if resp.status_code != 200:
                j = resp.json()
                return Error("%s: %s" % (resp.status_code, j["message"]))
        except:
            #traceback.print_exc()
            return Error("Attachment.update: unexpected error")

class Comment(Content):
    """Comment object (with Page parent).
    """

    def __init__(self, page):
        Content.__init__(self.page.get_site())
        self.page = page

    def update(self, content, data=None):
        return Error("not implemented")
