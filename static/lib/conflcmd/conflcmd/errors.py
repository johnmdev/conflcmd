#! /usr/bin/env python
#
# errors.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

"""Base class for Error and helper functions.

Provides an alternative to minimally differentiated integer exit
codes and exceptions.
"""

import sys

class Error:
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)

    def __repr__(self):
        return """<Error msg="%s">""" % str(self.msg)

def __perror(erro):
    sys.stderr.write("%s\n" % str(erro))

def is_error(erro):
    return isinstance(erro, Error)

def onerror_exit(erro, exitcode=1):
    if is_error(erro):
        sys.exit(exitcode)

def onerror_raise(erro, exccls=Exception):
    if is_error(erro):
        raise exccls(str(erro))

def perror(erro):
    if is_error(erro):
        __perror(erro)

def perror_exit(erro, exitcode=1):
    """On Error, write string to stderr. Exit on non-zero exitcode.
    """
    if is_error(erro):
        __perror(erro)
        if exitcode != 0:
            sys.exit(exitcode)

def perror_raise(erro, exccls=Exception):
    """On Error, write string to stderr. Raise exception on non-None exccls.
    """
    if is_error(erro):
        __perror(erro)
        if exccls != None:
            raise exccls(str(erro))
