#! /usr/bin/env python2
#
# conflcmd.py

# Copyright (c) 2019 John Marshall. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import sys

# encoding hack
reload(sys)
sys.setdefaultencoding("utf-8")

import sys as _sys
_sys.dont_write_bytecode = True

try:
    # make sure requests package is available
    import requests
    del requests
except:
    sys.stderr.write("""error: could not load "requests" package\n""")
    sys.exit(1)

from ConfigParser import ConfigParser
import getpass
import os
import os.path
from sys import stderr
import traceback

from conflcmd import Site, Content
from conflcmd.errors import Error, is_error, onerror_exit, onerror_raise, perror_exit, perror_raise

MAINCONF_PATH = os.path.expanduser("~/.conflcmd/main.conf")
CREDSCONF_PATH = os.path.expanduser("~/.conflcmd/creds.conf")

#
conf = None
site = None
password = None
serverurl = None
username = None

HIDE = """
--enpass            Load password from enpass file (~/.conflcmd/enpass).
                    The source of the passphrase is determined by
                    environment variables:
                    CONFLCMD_ENPASSPH - use value environment variable
                    CONFLCMD_ENPASSPHFILE - read from file
--envpass           Load password from environment variable
                    CONFLCMD_ENVPASS.
"""

def print_usage():
    print """\
usage: conflcmd [<options>] <serverurl> <cmd> [<args>]

Manage content on Confluence site. Unless indicated, if <path> is
"-", input is read from stdin.

Configuration files:
~/.conflcmd/main.conf
                    Main configuration.
~/.conflcmd/creds.conf
                    Credentials-only configuration. Note: should
                    only be readable by user.

Common options:
-p                  Load password from configuration file.
-P                  Ask for password.
-u <username>       Account to use.

Commands:
add-attachment <spacekey> <pagename> <attachmentname> <path> <comment>
    Add attachment.

create-page <spacekey> <parentpagename> <pagename> <path>
    Create page. Set parentpagename to "" to create top-level page.

delete-attachment <spacekey> <pagename> <attachmentname>
    Delete attachment.

delete-page <spacekey> <pagename>
    Delete page.

delete-page-version <spacekey> <pagename> <versionrange>
    Delete page versions (from page history). Version range is
    inclusive and takes the form: <first>[-<last>].

get-attachment <spacekey> <pagename> <attachmentname>
    Get attachment.

get-page <spacekey> <pagename>
    Get page contents.

get-page-version-count <spacekey> <pagename>
    Get count of page versions (latest included).

get-page-view <spacekey> <pagename>
    Get rendered page contents.

list-attachments <spacekey> <pagename>
    List page attachments.

list-pages <spacekey>
    List pages.

list-spaces
    List space keys.

update-attachment <spacekey> <pagename> <attachmentname> <path> <comment>
    Update existing attachment.

update-page <spacekey> <pagename> <path> <comment>
    Update existing page."""

def get_content(path):
    """Helper to get content from path for stdin.
    """
    if path == "-":
        return sys.stdin.read()
    else:
        return open(path).read()

def add_attachment(spacekey, pagename, attachmentname, path, comment):
    """Add attachment to a page.
    """
    ev = 0
    try:
        content = get_content(path)

        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        erro = page.add_attachment(attachmentname, content, comment)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def create_page(spacekey, parentpagename, pagename, path):
    """Create page. Set parentpagename to "" to create top-level
    page.
    """
    ev = 0
    try:
        content = get_content(path)

        space = site.get_space(spacekey)
        perror_exit(space)

        erro = space.create_page(parentpagename, pagename, content)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def delete_attachment(spacekey, pagename, attachmentname):
    """Delete attachment.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        erro = page.delete_attachment(attachmentname)
        perror_exit(page)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def delete_page(spacekey, pagename):
    """Delete page.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        erro = space.delete_page(pagename)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def delete_page_version(spacekey, pagename, versionrange):
    """Delete page version (from page history).
    """
    ev = 0
    try:
        l = versionrange.split("-", 1)
        first = int(l[0])
        if len(l) > 1:
            last = int(l[1])
        else:
            last = first

        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        count = page.get_version_count()
        if first < 0:
            perror_exit(Error("version range (%s) starts lower than 0" % (versionrange,)))
        if first > last:
            perror_exit(Error("version range (%s) start (%s) greater than end (%s)" % (versionrange, first, last)))
        if last > count:
            perror_exit(Error("version range (%s) outside number of versions (%s)" % (versionrange, count)))

        # delete "first" repeatedly
        for i in range(first, last+1):
            erro = page.delete_version(str(first))
            perror_exit(erro)
    except:
        traceback.print_exc()
        ev = 1
    sys.exit(ev)

def get_attachment(spacekey, pagename, attachmentname):
    """Get attachment.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        att = get_attachment(attachmentname)
        perror_exit(space)

        json = att.get_json()
        dllink = json["_links"]["download"]
        print site.get_conn().get(dllink).text
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def get_page(spacekey, pagename):
    """Get page content.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        s = page.get_content()
        if s == None:
            ev = 1
        else:
            print s
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def get_page_version_count(spacekey, pagename):
    """Get page version count.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        count = page.get_version_count()
        if count == None:
            ev = 1
        else:
            print "%s" % count
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def get_page_view(spacekey, pagename):
    """Get rendered page content.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        s = page.get_view()
        if s == None:
            ev = 1
        else:
            print s
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def list_attachments(spacekey, pagename):
    """List attachment names.
    """
    ev = 0
    try:
        names = site.get_space(spacekey).get_page(pagename).get_attachment_names()
        print "\n".join(names)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def list_pages(spacekey):
    """List page names from a space.
    """
    ev = 0
    try:
        names = site.get_space(spacekey).get_page_names()
        print "\n".join(names)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def list_spaces():
    """List space keys.
    """
    ev = 0
    try:
        names = site.get_space_keys()
        print "\n".join(names)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def purge_page(spacekey, pagename):
    """Purge page (from trash).
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        erro = space.purge_page(pagename)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def update_attachment(spacekey, pagename, attachmentname, path, comment):
    """Update attachment content of a page.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page = space.get_page(pagename)
        perror_exit(page)

        att = page.get_attachment(attachmentname)
        perror_exit(att)

        erro = att.update(get_content(path), comment)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def update_page(spacekey, pagename, path, comment):
    """Update page content.
    """
    ev = 0
    try:
        space = site.get_space(spacekey)
        perror_exit(space)

        page= space.get_page(pagename)
        perror_exit(page)

        erro = page.update(get_content(path), comment)
        perror_exit(erro)
    except:
        #traceback.print_exc()
        ev = 1
    sys.exit(ev)

def main():
    global site, password, serverurl, username

    try:
        askpassword = False
        enpass = False
        envpass = False
        loadpassword = False
        password = None
        serverurl = None
        passfile = None
        username = None

        args = sys.argv[1:]

        while args:
            arg = args.pop(0)
            if arg in ["-h", "--help"] and not args:
                print_usage()
                sys.exit(0)
            elif arg == "--enpass":
                enpass = True
            elif arg == "--envpass":
                envpass = True
            elif arg == "-P":
                askpassword = True
            elif arg == "-p":
                loadpassword = True
            elif arg == "-u" and args:
                username = args.pop(0)
            elif arg == "--passfile" and args:
                passfile = args.pop(0)
            else:
                serverurl = arg
                break

        if serverurl == None:
            raise Exception()

        # load configuration file(s)
        try:
            conf = ConfigParser()
            conf.read(MAINCONF_PATH)
            if os.path.exists(CREDSCONF_PATH):
                st = os.stat(CREDSCONF_PATH)
                if st.st_mode & 077:
                    stderr.write("error: fix credentials configuration file to be readable by user only\n")
                    sys.exit(1)
            conf.read(CREDSCONF_PATH)
        except:
            stderr.write("error: problem reading configuration file(s)\n")
            sys.exit(1)

        # get password
        if askpassword:
            if not username:
                raise Exception()
            password = getpass.getpass()
        elif loadpassword:
            # find password; section url and username must match
            for secname in conf.sections():
                try:
                    if conf.get(secname, "url") == serverurl \
                        and conf.get(secname, "username") == username:
                        password = conf.get(secname, "password")
                        break
                except:
                    pass
        elif enpass:
            if not username:
                raise Exception()
            if "CONFLCMD_ENPASSPH" in os.environ:
                passph = os.environ["CONFLCMD_ENPASSPH"]
            elif "CONFLCMD_ENPASSPHFILE" in os.environ:
                passph = open(os.environ["CONFLCMD_ENPASSPHFILE"]).read()
            password = getenpass(serverurl, username, passph)
            if password == None:
                stderr.write("error: cannot get enpass password\n")
                sys.exit(1)
        elif envpass:
            password = os.environ.get("CONFLCMD_ENVPASS")
            if password == None:
                stderr.write("error: cannot get envpass\n")
                sys.exit(1)
        elif passfile:
            try:
                password = open(passfile).read()
            except:
                stderr.write("error: cannot load passfile\n")
                sys.exit(1)

        site = Site(serverurl, username, password)
        password = None
    except SystemExit:
        raise
    except:
        #traceback.print_exc()
        stderr.write("error: bad or missing argument\n")
        sys.exit(1)

    try:
            arg = args.pop(0)
            if arg == "add-attachment":
                add_attachment(*args)
            elif arg == "create-page":
                create_page(*args)
            elif arg == "delete-attachment":
                delete_attachment(*args)
            elif arg == "delete-page":
                delete_page(*args)
            elif arg == "delete-page-version":
                delete_page_version(*args)
            elif arg == "get-attachment":
                get_attachment(*args)
            elif arg == "get-page":
                get_page(*args)
            elif arg == "get-page-version-count":
                get_page_version_count(*args)
            elif arg == "get-page-view":
                get_page_view(*args)
            elif arg == "list-attachments":
                list_attachments(*args)
            elif arg == "list-pages":
                list_pages(*args)
            elif arg == "list-spaces":
                list_spaces(*args)
            elif arg == "purge-page":
                purge_page(*args)
            elif arg == "update-attachment":
                update_attachment(*args)
            elif arg == "update-page":
                update_page(*args)
            else:
                raise Exception()
    except SystemExit:
        raise
    except:
        traceback.print_exc()
        stderr.write("error: bad or missing argument\n")
        sys.exit(1)

if __name__ == "__main__":
    main()
