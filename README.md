# conflcmd

Command line interface to managing Confluence content.

# Contact and Other Information

* Author: John Marshall
* Repository: https://bitbucket.org/johnmdev/conflcmd
* Web: https://expl.info/display/MISC/conflcmd+-+Managing+Confluence+Content
